from django.apps import AppConfig


class AblageConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ablage"
