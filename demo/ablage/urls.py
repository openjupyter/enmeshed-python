
from django.urls import path
from .views import index, connection_code, check_acceptance, clear_session, accept_relationship, relationship_details, get_user_data

urlpatterns = [
    path("", index, name="index"),
    path("api/connectionCode", connection_code),
    path("api/checkAcceptance", check_acceptance),
    path("api/acceptRelationship", accept_relationship),
    path("api/userData", get_user_data),
    path("clearSession", clear_session),
    path("details", relationship_details),
]
