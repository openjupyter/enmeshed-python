import json
import logging
from datetime import datetime, timedelta, timezone

import qrcode
import qrcode.image.svg
from django.conf import settings
from django.http import HttpRequest, JsonResponse
from django.shortcuts import redirect, render
from django.views.decorators.http import require_POST
from enmeshed_python.client import ConnectorClient
from enmeshed_python.types import (
    CanCreateOutgoingRequestRequest,
    ConnectorRelationshipTemplateContent,
)

log = logging.getLogger(__name__)


# Create your views here.
def index(request: HttpRequest):
    return render(request, "ablage/index.html", {})


def connection_code(request: HttpRequest):
    client = client_from_settings()

    rel_template = request.session.get("rel_template", {})
    use_cached = False
    if rel_template:
        expires_at = rel_template.get("expiresAt")
        log.info("info")
        log.warning("warn")
        log.info(
            "datetime.fromisoformat(expires_at): %r", datetime.fromisoformat(expires_at)
        )
        log.info("datetime.now(): %r", datetime.now(timezone.utc))
        if expires_at and datetime.fromisoformat(expires_at) > datetime.now(
            timezone.utc
        ):
            use_cached = True

    if use_cached:
        url = rel_template.get("url")
    else:
        # communicate with connector
        ccr_arg = CanCreateOutgoingRequestRequest.model_validate_json(REQUEST_TEMPLATE)
        log.info("-> Calling can_create_request")
        validation_res = client.outgoing_requests.can_create_request(ccr_arg)

        if not validation_res.isSuccess:
            return

        log.info("-> Calling create_own_relationship_template")
        new_template = client.relationship_templates.create_own_relationship_template(
            maxNumberOfAllocations=1,
            expiresAt=(datetime.now() + timedelta(hours=2)).isoformat(),
            content=ConnectorRelationshipTemplateContent(
                title="Connector Demo Contact",
                onNewRelationship=ccr_arg.content,
                onExistingRelationship=ccr_arg.content,
            ),
        )

        url = f"nmshd://tr#{new_template.truncatedReference}"

        rel_template = {
            "id": new_template.id,
            "expiresAt": new_template.expiresAt,
            "url": url,
            "scanned": False,
        }
        request.session["rel_template"] = rel_template

    img = qrcode.make(url, image_factory=qrcode.image.svg.SvgPathImage)

    result = {
        "url": url,
        "templateId": rel_template.get("id"),
        "svgsource": img.to_string().decode("utf-8"),
    }
    log.info("result: %r", result)

    return JsonResponse(result)


@require_POST
def clear_session(request: HttpRequest):
    request.session["rel_template"] = {}
    request.session["peer_id"] = None
    return redirect("index")


def check_acceptance(request: HttpRequest):

    rel_template = request.session.get("rel_template", {})
    if not rel_template:
        return JsonResponse(
            {"status": "error", "message": "No relationship template found in session."}
        )

    # force a sync
    client = client_from_settings()
    sync_result = client.accounts.sync()
    print()
    print("Sync result:")
    print(sync_result)

    # on the first time, we only have the relationship template id.
    # For repeat connections, we should have the relationship id or the peer id.

    # on acceptance, we get something like this:
    # messages=[ConnectorMessage(id='MSGfElEUoqTwezFMIHLc', content=ResponseWrapper(type='ResponseWrapper', requestId='REQEpEDRbHApXdQ9LTXd', requestSourceReference='RLThpdJwAHZMyXfyPon0', requestSourceType='RelationshipTemplate', response=ResponseWrapper_Response(items=[ResponseItemGroup(type='ResponseItemGroup', items=[ShareAttributeAcceptResponseItem(type='ShareAttributeAcceptResponseItem', attributeId='ATTWoZYFNfS8cYmeLmpV', result='Accepted')]), ResponseItemGroup(type='ResponseItemGroup', items=[ReadAttributeAcceptResponseItem(type='ReadAttributeAcceptResponseItem', attribute=IdentityAttribute(type='IdentityAttribute', owner='id19GA7qaLxkou4VUDJieP5xqS1AqbuwUeG6', value=GivenName(type='GivenName', value='Bingo ')), attributeId='ATTPNlX9STtACWaDturu', result='Accepted'), ReadAttributeAcceptResponseItem(type='ReadAttributeAcceptResponseItem', attribute=IdentityAttribute(type='IdentityAttribute', owner='id19GA7qaLxkou4VUDJieP5xqS1AqbuwUeG6', value=Surname(type='Surname', value='Dongo')), attributeId='ATTiuVEYNcxDrzqAihbC', result='Accepted'), RejectResponseItem(type='RejectResponseItem', result='Rejected')])])), createdBy='id19GA7qaLxkou4VUDJieP5xqS1AqbuwUeG6', createdByDevice='DVCJlN7UlO9zwkMjejN7', recipients=[ConnectorMessageRecipient(address='id1HE5DcHw2vCHvRw7i6tQ98CjbnJuGFe9Fa', relationshipId='RELBF0nFJdBUum9BRBju', receivedAt='2024-03-20T08:53:02.121Z', receivedByDevice='DVC5ktGfhQV9TDeI8Vol')], createdAt='2024-03-20T08:52:56.189Z', attachments=[], isOwn=False)] relationships=[]

    # TODO: reading the result of sync only works when there is a single user,
    # or when we use some kind of message queue!
    template_id = rel_template.get("id")
    for msg in sync_result.messages:
        if msg.content.requestSourceReference == template_id:
            print("Found message!")
            request.session["rel_template"]["scanned"] = True
            request.session["peer_id"] = msg.createdBy
            return JsonResponse(
                {
                    "status": "accepted",
                    "message": "Relationship accepted.",
                    "peer_id": msg.createdBy,
                }
            )

    if request.session.get("peer_id"):
        return JsonResponse(
            {
                "status": "accepted",
                "message": "Relationship previously accepted.",
                "peer_id": request.session.get("peer_id"),
            }
        )

    # The code below only works on the first template the user scans!

    rels = client.relationships.get_relationships(template_id=template_id)

    for rel in rels:
        log.info("Relationship id: %s, status: %s", rel.id, rel.status)

    # there should be only one:
    if len(rels) == 0:
        return JsonResponse({"status": "waiting", "message": "No relationship found."})

    rel = rels[0]
    request.session["peer_id"] = rel.peerIdentity.address
    for change in rel.changes:
        log.info("Change id: %s, status: %s", change.id, change.status)
        if change.response:
            content = change.response.content
        else:
            content = None
        log.info("Content: %r", content)
    return JsonResponse({"status": "accepted", "message": "Relationship found."})


def accept_relationship(request: HttpRequest):
    rel_template = request.session.get("rel_template", {})
    if not rel_template:
        return JsonResponse(
            {"status": "error", "message": "No relationship template found in session."}
        )

    template_id = rel_template.get("id")

    client = client_from_settings()
    rels = client.relationships.get_relationships(template_id=template_id)

    for rel in rels:
        log.info("Relationship id: %s, status: %s", rel.id, rel.status)

    # there should be only one:
    if len(rels) == 0:
        return JsonResponse({"status": "error", "message": "No relationship found."})

    rel = rels[0]
    for change in rel.changes:
        log.info("Change id: %s, status: %s", change.id, change.status)
        if change.status == "Pending":
            log.info("-> Calling accept_relationship_change")
            client.relationships.accept_relationship_change(
                relationship_id=rel.id, change_id=change.id
            )

    return JsonResponse({"status": "ok", "message": "Relationship accepted."})


def get_user_data(request: HttpRequest) -> JsonResponse:
    client = client_from_settings()
    user_peer_id = request.session["peer_id"]
    assert isinstance(user_peer_id, str)

    attrs = client.attributes.get_peer_shared_identity_attributes(peer=user_peer_id)
    attrs.sort(key=lambda x: x.createdAt)

    clean_attrs = {}
    for attr in attrs:
        clean_attrs[attr.content.value.type] = attr.content.value.value

    result = {"peerId": user_peer_id, "attributes": clean_attrs}
    return JsonResponse(result)


def relationship_details(request: HttpRequest):
    user_peer_id = request.session.get("peer_id")
    if not user_peer_id:
        return JsonResponse(
            {"status": "error", "message": "No peer id found in session."}
        )
    client = client_from_settings()

    # Get all relationships with this user. Can there be multiple?
    # rels = client.relationships.get_relationships(peer=user_peer_id)
    # relationship_json = TypeAdapter(ConnectorRelationships).dump_json(rels, indent=2, by_alias=True, exclude_unset=True).decode('utf-8') #json.dumps(rels, indent=2)

    attrs = client.attributes.get_peer_shared_identity_attributes(peer=user_peer_id)
    # dumped = TypeAdapter(ConnectorAttributes).dump_json(attrs, indent=2, by_alias=True, exclude_unset=True).decode('utf-8')
    print(attrs)
    attrs.sort(key=lambda x: x.createdAt)

    clean_attrs = {}
    for attr in attrs:
        clean_attrs[attr.content.value.type] = attr.content.value.value

    dumped = json.dumps(clean_attrs, indent=2)

    return render(
        request, "ablage/relationship_details.html", {"relationship_json": dumped}
    )


def client_from_settings():
    """ Builds a connector API client from the settings. """

    return ConnectorClient(
        settings.WALLET_CONNECTOR_BASE_URL,
        settings.WALLET_CONNECTOR_API_KEY
    )


REQUEST_TEMPLATE = """{
  "content": {
    "items": [
      {
        "@type": "RequestItemGroup",
        "mustBeAccepted": true,
        "title": "Shared Attributes",
        "items": [
          {
            "@type": "ShareAttributeRequestItem",
            "mustBeAccepted": true,
            "attribute": {
              "@type": "IdentityAttribute",
              "owner": "id1HE5DcHw2vCHvRw7i6tQ98CjbnJuGFe9Fa",
              "value": {
                "@type": "DisplayName",
                "value": "Demo Connector of OpenJupyter"
              }
            },
            "sourceAttributeId": "ATTi8ctxAEcbEp2eoie3"
          }
        ]
      },
      {
        "@type": "RequestItemGroup",
        "mustBeAccepted": true,
        "title": "Requested Attributes",
        "items": [
          {
            "@type": "ReadAttributeRequestItem",
            "mustBeAccepted": true,
            "query": {
              "@type": "IdentityAttributeQuery",
              "valueType": "GivenName"
            }
          },
          {
            "@type": "ReadAttributeRequestItem",
            "mustBeAccepted": true,
            "query": {
              "@type": "IdentityAttributeQuery",
              "valueType": "Surname"
            }
          },
          {
            "@type": "ReadAttributeRequestItem",
            "mustBeAccepted": false,
            "query": {
              "@type": "IdentityAttributeQuery",
              "valueType": "EMailAddress"
            }
          }
        ]
      }
    ]
  }
}"""
