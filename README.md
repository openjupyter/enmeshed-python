
Experimental Python API for the Enmeshed API / Mein Bildungsraum.

This is based on the [Enmeshed Connector TypeScript SDK](https://github.com/nmshd/connector/tree/main/packages/sdk).  Currently, only a subset of endpoints are supported. Mainly for the flow of creating a relationship with a Wallet via QR code and exchanging attributes.

For type safety, we use [Pydantic](https://docs.pydantic.dev/latest/). This lets us create an API that is very close to the TypeScript API.

## Installation

Installation is like a regular Python package. As long as it is not on PyPi, you can clone this repository and install from the local directory:

```bash
pip install -e .
```

## Demonstrator

A simple demonstration is available as a Django app in the demo directory.