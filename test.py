from __future__ import annotations

import logging
import logging.config
from datetime import datetime, timedelta

from config import API_KEY, BASE_URL
from enmeshed_python.client import ConnectorClient
from enmeshed_python.types import (CanCreateOutgoingRequestRequest,
                                   ConnectorRelationshipTemplateContent,
                                   GetOutgoingRequestsRequest,
                                   CreateOwnRelationshipTemplateRequest, )

log = logging.getLogger(__name__)


def main():
    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger("httpx").setLevel(logging.ERROR)
    logging.getLogger("httpcore").setLevel(logging.ERROR)

    client = ConnectorClient(BASE_URL, API_KEY)
    health = client.monitoring.get_health()
    print(health)

    sync_result = client.accounts.sync()
    print(sync_result.model_dump_json())

    rels = client.relationships.get_relationships(template_id="RLTG50zNE4mOZaRCk2Ws")
    for rel in rels:
        print(rel.id, rel.status)
        print(rel.model_dump_json())

    return

    # # args = GetOutgoingRequestsRequest.model_validate(
    # #     {'status': "Draft", 'content': {'expiresAt': '2024-12-31T00:00:00.000Z'}})
    # args = GetOutgoingRequestsRequest.model_validate({'status': "Draft"})
    # reqs = client.outgoing_requests.get_requests(args)
    # for req in reqs:
    #     print(req.id, req.status)
    #     print(req.model_dump_json())
    # # print(reqs)

    # rels = client.relationships.get_relationships()  # status="Active")
    # for rel in rels:
    #     print()
    #     print("Relationship:", rel.id, rel.status)
    #     print("Changes:")
    #     for change in rel.changes:
    #         print(" ", change.id, change.status)
    #         #print(change.model_dump_json())
    #     print()
    #     print(rel.model_dump_json())

    # return

    ccr_arg = CanCreateOutgoingRequestRequest.model_validate_json(
        REQUEST_TEMPLATE)
    log.info("-> Calling can_create_request")
    validation_res = client.outgoing_requests.can_create_request(ccr_arg)

    if not validation_res.isSuccess:
        return

    tr = CreateOwnRelationshipTemplateRequest(
        maxNumberOfAllocations=1,
        expiresAt=(datetime.now() + timedelta(hours=2)).isoformat(),
        # expiresAt="2024-12-31T00:00:00.000Z",
        content=ConnectorRelationshipTemplateContent(
            title="Connector Demo Contact",
            onNewRelationship=ccr_arg.content,
            onExistingRelationship=ccr_arg.content
        ),
    )
    log.info("-> Calling create_own_relationship_template")
    new_template = client.relationship_templates.create_own_relationship_template(tr)
    log.info("-> Calling get_qr_code_for_own_relationship_template")
    client.relationship_templates.get_qr_code_for_own_relationship_template(new_template.id)


# Request template from Enmeshed Connector tutorial
REQUEST_TEMPLATE = """{
  "content": {
    "items": [
      {
        "@type": "RequestItemGroup",
        "mustBeAccepted": true,
        "title": "Shared Attributes",
        "items": [
          {
            "@type": "ShareAttributeRequestItem",
            "mustBeAccepted": true,
            "attribute": {
              "@type": "IdentityAttribute",
              "owner": "id1HE5DcHw2vCHvRw7i6tQ98CjbnJuGFe9Fa",
              "value": {
                "@type": "DisplayName",
                "value": "Demo Connector of OpenJupyter"
              }
            },
            "sourceAttributeId": "ATTi8ctxAEcbEp2eoie3"
          }
        ]
      },
      {
        "@type": "RequestItemGroup",
        "mustBeAccepted": true,
        "title": "Requested Attributes",
        "items": [
          {
            "@type": "ReadAttributeRequestItem",
            "mustBeAccepted": true,
            "query": {
              "@type": "IdentityAttributeQuery",
              "valueType": "GivenName"
            }
          },
          {
            "@type": "ReadAttributeRequestItem",
            "mustBeAccepted": true,
            "query": {
              "@type": "IdentityAttributeQuery",
              "valueType": "Surname"
            }
          },
          {
            "@type": "ReadAttributeRequestItem",
            "mustBeAccepted": false,
            "query": {
              "@type": "IdentityAttributeQuery",
              "valueType": "EMailAddress"
            }
          }
        ]
      }
    ]
  }
}"""

if __name__ == "__main__":
    main()
