
from __future__ import annotations

from enmeshed_python.types.relationships import ConnectorRelationshipChange
from pydantic import TypeAdapter
from typing import Optional, Union
import pytest
from pydantic import ValidationError
from enmeshed_python.types.relationships import ConnectorRelationshipChangeRequest
from enmeshed_python.types.unsorted import ShareAttributeAcceptResponseItem, ShareAttributeRejectResponseItem, RelationshipCreationChangeRequestContent, RelationshipCreationChangeRequestContent_Response

CHANGE_EXAMPLE = '{"id":"RCHCaiXduxKcrdDSpqse","request":{"createdBy":"id1L4cQ35f8QRprX413wMPkTP3RhJLdK9PHg","createdByDevice":"DVCWGpcuLfeUsWmwsYuI","createdAt":"2024-03-19T09:15:10.326Z","content":{"@type":"RelationshipCreationChangeRequestContent","response":{"items":[{"@type":"ResponseItemGroup","items":[{"@type":"ShareAttributeAcceptResponseItem","attributeId":"ATTK8GspfSiFRdz17gd3","result":"Accepted"}]},{"@type":"ResponseItemGroup","items":[{"@type":"ReadAttributeAcceptResponseItem","attribute":{"@type":"IdentityAttribute","owner":"id1L4cQ35f8QRprX413wMPkTP3RhJLdK9PHg","value":{"@type":"GivenName","value":"Test"}},"attributeId":"ATTU1gwLC9hTUCGbStJQ","result":"Accepted"},{"@type":"ReadAttributeAcceptResponseItem","attribute":{"@type":"IdentityAttribute","owner":"id1L4cQ35f8QRprX413wMPkTP3RhJLdK9PHg","value":{"@type":"Surname","value":"User"}},"attributeId":"ATTCzyuydQD0UhfvUqmR","result":"Accepted"},{"@type":"RejectResponseItem","result":"Rejected"}]}],"requestId":"REQoZgaz1Ej59BpDd8MS","result":"Accepted"}}},"status":"Pending","type":"Creation"}'

# def test_parse_change():
#     ada = TypeAdapter(ConnectorRelationshipChange)
#     result = ada.validate_json(CHANGE_EXAMPLE, strict=False)
#     assert result.id == "RCHCaiXduxKcrdDSpqse"
#     assert result.request.content
#     print(type(result.request.content))

from enmeshed_python.types.attypes import *

def test_attype_parsing():
    test_data = {"@type": "ShareAttributeAcceptResponseItem", "attributeId": "ATTK8GspfSiFRdz17gd3", "result": "Accepted"}
    parsed = my_factory(test_data)
    assert isinstance(parsed, ShareAttributeAcceptResponseItem)


def test_attype_dump():
    test_data = {"@type": "ShareAttributeAcceptResponseItem", "attributeId": "ATTK8GspfSiFRdz17gd3", "result": "Accepted"}
    parsed = my_factory(test_data)
    #assert isinstance(parsed, ShareAttributeAcceptResponseItem)
    dumped = parsed.model_dump(by_alias=True)
    # print(dumped)
    assert dumped["@type"] == "ShareAttributeAcceptResponseItem"
    assert dumped["attributeId"] == "ATTK8GspfSiFRdz17gd3"
    assert dumped["result"] == "Accepted"
    assert dumped.keys() == test_data.keys()

def test_attype_create():
    obj = ShareAttributeAcceptResponseItem(attributeId="ATTK8GspfSiFRdz17gd3", result="Accepted")
    assert obj.attributeId == "ATTK8GspfSiFRdz17gd3"
    assert obj.result == "Accepted"
    # assert obj["@type"] == "ShareAttributeAcceptResponseItem"
    assert obj.type == "ShareAttributeAcceptResponseItem"


class Fruit(AtType):
    type: Literal["Fruit"] = Field("Fruit", alias="@type")
    name: str

class Car(AtType):
    type: Literal["Car"] = Field("Car", alias="@type")
    model: str

def test_find_out_type():
    car_data = {"@type": "Car", "model": "BMW"}
    car_parsed = AtType.model_validate(car_data)
    assert isinstance(car_parsed, Car)
    assert car_parsed.type == "Car"
    assert car_parsed.model == "BMW"

    fruit_data = {"@type": "Fruit", "name": "Apple"}
    fruit_parsed = TypeAdapter(AtType).validate_python(fruit_data)
    assert isinstance(fruit_parsed, Fruit)
    assert fruit_parsed.type == "Fruit"
    assert fruit_parsed.name == "Apple"


    invalid_data = {"@type": "invalid", "name": "Apple"}
    with pytest.raises(ValidationError):
        AtType.model_validate(invalid_data)
    

class ContainerType(BaseModel):
    child: Union[AtType, Any]

def test_nested_attype():
    data = {"child": {"@type": "Fruit", "name": "Apple"}}
    parsed = ContainerType.model_validate(data)
    assert isinstance(parsed.child, Fruit)

def test_nested_attype_any():
    data = {"child": {"random": 123}}
    parsed = ContainerType.model_validate(data)
    assert isinstance(parsed.child, dict)
    assert parsed.child["random"] == 123

class Beverage(AtType):
    type: Literal["Beverage"] = Field("Beverage", alias="@type")
    taste: AtType

class SimpleBeverage(BaseModel):
    taste: AtType

def test_nested():
    data = {"@type": "Beverage", "taste": {"@type": "Fruit", "name": "Apple"}}
    parsed = Beverage.model_validate(data)
    assert isinstance(parsed, Beverage)
    assert isinstance(parsed.taste, Fruit)

def test_nested_parse_as_attype():
    data = {"@type": "Beverage", "taste": {"@type": "Fruit", "name": "Apple"}}
    parsed = AtType.model_validate(data)
    assert isinstance(parsed, Beverage)
    assert isinstance(parsed.taste, Fruit)

def test_nested_in_basemodel():
    data = {"taste": {"@type": "Fruit", "name": "Apple"}}
    parsed = SimpleBeverage.model_validate(data)
    assert isinstance(parsed, SimpleBeverage)
    assert isinstance(parsed.taste, Fruit)

def test_nested_in_basemodel_from_json():
    json_data = '{"taste": {"@type": "Fruit", "name": "Apple"}}'
    parsed = SimpleBeverage.model_validate_json(json_data)
    assert isinstance(parsed, SimpleBeverage)
    assert isinstance(parsed.taste, Fruit)

class FruitBasket(BaseModel):
    fruits: List[Fruit]

def test_list_in_basemodel():
    data = {"fruits": [{"@type": "Fruit", "name": "Apple"}, {"@type": "Fruit", "name": "Banana"}]}
    parsed = FruitBasket.model_validate(data)
    assert isinstance(parsed, FruitBasket)
    assert len(parsed.fruits) == 2
    assert isinstance(parsed.fruits[0], Fruit)

def test_list_in_basemodel_json():
    data = '{"fruits": [{"@type": "Fruit", "name": "Apple"}, {"@type": "Fruit", "name": "Banana"}]}'
    parsed = FruitBasket.model_validate_json(data)
    assert isinstance(parsed, FruitBasket)
    assert len(parsed.fruits) == 2
    assert isinstance(parsed.fruits[0], Fruit)

class AnyBasket(BaseModel):
    fruits: List[AtType]

def test_attype_list_in_basemodel():
    data = {"fruits": [{"@type": "Fruit", "name": "Apple"}, {"@type": "Fruit", "name": "Banana"}]}
    parsed = AnyBasket.model_validate(data)
    assert isinstance(parsed, AnyBasket)
    assert len(parsed.fruits) == 2
    assert isinstance(parsed.fruits[0], Fruit)

def test_attype_list_in_basemodel_json():
    data = '{"fruits": [{"@type": "Fruit", "name": "Apple"}, {"@type": "Fruit", "name": "Banana"}]}'
    parsed = AnyBasket.model_validate_json(data)
    assert isinstance(parsed, AnyBasket)
    assert len(parsed.fruits) == 2
    assert isinstance(parsed.fruits[0], Fruit)

# def test_ConnectorRelationshipChange():
#     change = ConnectorRelationshipChange.model_validate_json(CHANGE_EXAMPLE)
#     assert change.id == "RCHCaiXduxKcrdDSpqse"
#     assert change.request.createdBy == "id1L4cQ35f8QRprX413wMPkTP3RhJLdK9PHg"
#     assert isinstance(change.request.content, RelationshipCreationChangeRequestContent)
#     # assert type(change.request.content).__name__ == "RelationshipCreationChangeRequestContent"
#     print(type(change.request.content))
#     assert isinstance(change.request.content.response, RelationshipCreationChangeRequestContent_Response)
#     print(type(change.request.content.response))
#     # assert len(change.request.content.response.items) == 2

def test_ConnectorRelationshipChange():
    data = {
        "id": "RCHCaiXduxKcrdDSpqse",
        "request": {
            "createdBy": "id1L4cQ35f8QRprX413wMPkTP3RhJLdK9PHg",
            "createdByDevice": "DVCWGpcuLfeUsWmwsYuI",
            "createdAt": "2024-03-19T09:15:10.326Z",
            "content": {
                "@type": "RelationshipCreationChangeRequestContent",
                "response": {
                    "items": [],
                    "requestId": "REQoZgaz1Ej59BpDd8MS",
                    "result": "Accepted"
                }
            }
        },
        "status": "Pending",
        "type": "Creation"
    }

    change = ConnectorRelationshipChange.model_validate(data)
    assert change.id == "RCHCaiXduxKcrdDSpqse"
    assert change.request.createdBy == "id1L4cQ35f8QRprX413wMPkTP3RhJLdK9PHg"
    assert isinstance(change.request.content, RelationshipCreationChangeRequestContent)


# like ConnectorRelationshipChange
class Level1(BaseModel):
    id: str
    request: Level2
    # response: Optional[Car] = None

# like ConnectorRelationshipChangeRequest
class Level2(BaseModel):
    createdBy: str
    content: Optional[AtType|Any] = None

# like RelationshipCreationChangeRequestContent
class Level3(AtType):
    type: Literal["Level3"] = Field(default="Level3", alias="@type")
    response: Level4

# like RelationshipCreationChangeRequestContent_Response
class Level4(BaseModel):
    items: List[Level5]
    requestId: str
    result: str

# like ResponseItemGroup
class Level5(AtType):
    type: Literal["Level5"] = Field(default="Level5", alias="@type")
    # items: List[Union[ShareAttributeAcceptResponseItem, ShareAttributeRejectResponseItem]]
    items: List[Level6|Any]

class Level6(AtType):
    type: Literal["Level6"] = Field(default="Level6", alias="@type")
    attributeId: str
    result: str


def test_ConnectorRelationshipChange_json():
    data = """{
    "id": "RCHCaiXduxKcrdDSpqse",
    "request": {
        "createdBy": "id1L4cQ35f8QRprX413wMPkTP3RhJLdK9PHg",
        "createdByDevice": "DVCWGpcuLfeUsWmwsYuI",
        "createdAt": "2024-03-19T09:15:10.326Z",
        "content": {
            "@type": "Level3",
            "response": {
                "items": [
                    {
                        "@type": "Level5",
                        "items": [
                            {
                                "@type": "Level6",
                                "attributeId": "ATTK8GspfSiFRdz17gd3",
                                "result": "Accepted"
                            }
                        ]
                    },
                    {
                        "@type": "Level5",
                        "items": [
                            {
                                "@type": "ReadAttributeAcceptResponseItem",
                                "attribute": {
                                    "@type": "IdentityAttribute",
                                    "owner": "id1L4cQ35f8QRprX413wMPkTP3RhJLdK9PHg",
                                    "value": {
                                        "@type": "GivenName",
                                        "value": "Test"
                                    }
                                },
                                "attributeId": "ATTU1gwLC9hTUCGbStJQ",
                                "result": "Accepted"
                            },
                            {
                                "@type": "ReadAttributeAcceptResponseItem",
                                "attribute": {
                                    "@type": "IdentityAttribute",
                                    "owner": "id1L4cQ35f8QRprX413wMPkTP3RhJLdK9PHg",
                                    "value": {
                                        "@type": "Surname",
                                        "value": "User"
                                    }
                                },
                                "attributeId": "ATTCzyuydQD0UhfvUqmR",
                                "result": "Accepted"
                            },
                            {
                                "@type": "RejectResponseItem",
                                "result": "Rejected"
                            }
                        ]
                    }
                ],
                "requestId": "REQoZgaz1Ej59BpDd8MS",
                "result": "Accepted"
            }
        }
    },
    "status": "Pending",
    "type": "Creation"
}"""

    change = Level1.model_validate_json(data)
    # change = TypeAdapter(ConnectorRelationshipChange).validate_json(data)
    assert change.id == "RCHCaiXduxKcrdDSpqse"
    assert change.request.createdBy == "id1L4cQ35f8QRprX413wMPkTP3RhJLdK9PHg"
    #assert isinstance(change.request.content, RelationshipCreationChangeRequestContent)
    # print("type(change.request.content):", type(change.request.content))
    assert type(change) == Level1
    assert type(change.request) == Level2
    assert type(change.request.content) == Level3
    
def test_ConnectorRelationshipChange_json2():
    change = ConnectorRelationshipChange.model_validate_json(CHANGE_EXAMPLE)
    assert change.id == "RCHCaiXduxKcrdDSpqse"
    assert change.request.createdBy == "id1L4cQ35f8QRprX413wMPkTP3RhJLdK9PHg"
    # print("type(change.request.content):", type(change.request.content))
    assert isinstance(change, ConnectorRelationshipChange)
    assert isinstance(change.request, ConnectorRelationshipChangeRequest)
    assert isinstance(change.request.content, RelationshipCreationChangeRequestContent)

def test_RelationshipCreationChangeRequestContent():
    data = {
        "@type": "RelationshipCreationChangeRequestContent",
        "response": {
            "items": [
                {
                    "@type": "ResponseItemGroup",
                    "items": [
                        {
                            "@type": "ShareAttributeAcceptResponseItem",
                            "attributeId": "ATTK8GspfSiFRdz17gd3",
                            "result": "Accepted"
                        }
                    ]
                },
            ],
            "requestId": "REQoZgaz1Ej59BpDd8MS",
            "result": "Accepted"
        }
    }
    parsed = RelationshipCreationChangeRequestContent.model_validate(data)
    assert isinstance(parsed, RelationshipCreationChangeRequestContent)
    assert isinstance(parsed.response, RelationshipCreationChangeRequestContent_Response)
    assert len(parsed.response.items) == 1
    

def test_RelationshipCreationChangeRequestContent2():
    data = {
        "@type": "RelationshipCreationChangeRequestContent",
        "response": {
            "items": [
                {
                    "@type": "ResponseItemGroup",
                    "items": [
                        {
                            "@type": "ShareAttributeAcceptResponseItem",
                            "attributeId": "ATTK8GspfSiFRdz17gd3",
                            "result": "Accepted"
                        }
                    ]
                },
            ],
            "requestId": "REQoZgaz1Ej59BpDd8MS",
            "result": "Accepted"
        }
    }
    parsed = AtType.model_validate(data)
    assert isinstance(parsed, RelationshipCreationChangeRequestContent)
    assert isinstance(parsed.response, RelationshipCreationChangeRequestContent_Response)
    assert len(parsed.response.items) == 1


def test_dump_list_includes_all_fields():
    data = [
        Fruit(name="Apple"),
        Fruit(name="Banana"),
        Fruit(name="Orange")
    ]
    adapter = TypeAdapter(List[Fruit])
    dumped = adapter.dump_python(data)
    assert len(dumped) == 3
    assert dumped[0]["name"] == "Apple"
    assert dumped[1]["name"] == "Banana"
    assert dumped[2]["name"] == "Orange"
    