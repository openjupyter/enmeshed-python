from dataclasses import dataclass

# from dataclasses_json import DataClassJsonMixin
from typing import Annotated, Any, Literal
from pydantic import BaseModel, Field, TypeAdapter


class ThingBase(BaseModel):
    typ: Literal["thing"]


class Fruit(ThingBase):
    typ: Literal["fruit"]
    name: str
    price: float


class Car(ThingBase):
    typ: Literal["car"]
    make: str
    model: str
    year: int


# Thing = TypeAdapter(ThingBase, {
#     'fruit': Fruit,
#     'car': Car
# })


Thing = TypeAdapter(Annotated[Fruit | Car, Field(discriminator="typ")])

one = Thing.validate_python({"typ": "fruit", "name": "apple", "price": 1.23})
two = Thing.validate_python(
    {"typ": "car", "make": "ford", "model": "focus", "year": 2007}
)
print(one)
print(repr(one))
print(two)
print(repr(two))
