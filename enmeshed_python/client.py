from __future__ import annotations

import logging

import httpx

from enmeshed_python.endpoints import (
    AccountEndpoint,
    AttributesEndpoint,
    MessagesEndpoint,
    MonitoringEndpoint,
    OutgoingRequestsEndpoint,
    RelationshipsEndpoint,
    RelationshipTemplatesEndpoint,
)

log = logging.getLogger(__name__)


class ConnectorClient:
    def __init__(self, base_url, api_key):
        self.base_url = base_url
        self.api_key = api_key
        headers = {"X-API-KEY": self.api_key}
        self.client = httpx.Client(base_url=self.base_url, headers=headers)
        self.outgoing_requests = OutgoingRequestsEndpoint(self.client)
        self.relationships = RelationshipsEndpoint(self.client)
        self.accounts = AccountEndpoint(self.client)
        self.messages = MessagesEndpoint(self.client)
        self.attributes = AttributesEndpoint(self.client)
        self.relationship_templates = RelationshipTemplatesEndpoint(self.client)
        self.monitoring = MonitoringEndpoint(self.client)
