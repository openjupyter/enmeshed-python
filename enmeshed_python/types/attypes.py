from __future__ import annotations

from functools import wraps
from typing import Any, Dict, List, Literal, cast

from pydantic import (BaseModel, ConfigDict, Field,
                      ValidatorFunctionWrapHandler, ValidationInfo,
                      create_model, field_validator, model_serializer,
                      model_validator, root_validator, validator)
from pydantic._internal._model_construction import ModelMetaclass
from typing_extensions import Unpack

type_mapping: Dict[str, BaseModel] = {
}

class AtTypeMeta(ModelMetaclass):
    def __new__(cls, name, bases, dct):
        new_cls = cast(BaseModel, super().__new__(cls, name, bases, dct))
        field = new_cls.model_fields["type"]
        key = field.default
        type_mapping[key] = new_cls
        return new_cls

class AtType(BaseModel, metaclass=AtTypeMeta):
    type: str = Field(alias="@type")

    # @root_validator
    # def determine_type(cls, values):
    #     if "@type" not in values:
    #         raise ValueError("Data must contain an '@type' field.")
    #     type_identifier = values["@type"]
    #     if type_identifier not in type_mapping:
    #         raise ValueError(f"Unknown type: {type_identifier}")
        
    #     return values

    @model_validator(mode="wrap")
    @classmethod
    def determine_type(cls, data: Any, handler: ValidatorFunctionWrapHandler, info: ValidationInfo) -> AtType:
        # print()
        # print("★ in determine_type %r, %r" % (cls, data))
        # print("  info.mode: %s" % (info.mode,))

        if not cls == AtType:
            # print()    
            # print("★ cls is of different type: %r" % (cls,))
            return handler(data)

        # print()
        # print("data is: %r" % (data,))
        # print("isinstance(data, cls): %r" % (isinstance(data, cls)))
        # print("isinstance(data, AtType): %r" % (isinstance(data, AtType)))

        if not isinstance(data, dict):
            # print()
            # print("★ data is not a dict: %r" % (data,))
            return handler(data)


        # # only run on AtType directly
        # if isinstance(data, AtType) and not isinstance(data, cls):
        #     return data
        

        # if isinstance(data, cls):
        #     print("Warning, not implemented")
        #     return data
        
        # if isinstance(data, dict):
        # print("data: %r" % (data,))
        if "@type" not in data:
            raise ValueError("Data must contain an '@type' field.")
            return handler(data)
        type_identifier = data["@type"]
        if type_identifier not in type_mapping:
            raise ValueError(f"Unknown type: {type_identifier}")
        klass = type_mapping[type_identifier]
        # print("klass: %r" % (klass,))
        #return klass(**data)
        return klass.model_validate(data)
        # return handler(klass.model_construct(**data))
        #return handler(data)

        


    # def __init_subclass__(cls, **kwargs):
    #     field = cls.model_fields["type"]
    #     key = field.default
    #     type_mapping[key] = cls
    #     return super().__init_subclass__(**kwargs)




# def at_type(cls):
#     field = cls.model_fields["type"]
#     key = field.default
#     type_mapping[key] = cls
#     return cls



def my_factory(data: Dict[str, Any]) -> BaseModel:
    if "@type" not in data:
        raise ValueError("Data must contain an '@type' field.")
    
    type_identifier = data["@type"]
    if type_identifier not in type_mapping:
        raise ValueError(f"Unknown type: {type_identifier}")
    
    model_class = type_mapping[type_identifier]
    # print("identified as: %r" % (model_class,))
    return model_class.model_construct(**data)
