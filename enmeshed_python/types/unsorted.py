from __future__ import annotations
from enmeshed_python.types.attypes import AtType, my_factory
from pydantic import Field, BaseModel, validator, field_validator
from typing import List, Union, Literal, Any


class ShareAttributeAcceptResponseItem(AtType):
    type: Literal["ShareAttributeAcceptResponseItem"] = Field(default="ShareAttributeAcceptResponseItem", alias="@type")
    attributeId: str
    result: str

class ShareAttributeRejectResponseItem(AtType):
    type: Literal["ShareAttributeRejectResponseItem"] = Field(default="ShareAttributeRejectResponseItem", alias="@type")
    attributeId: str
    result: str

class RejectResponseItem(AtType):
    type: Literal["RejectResponseItem"] = Field(default="RejectResponseItem", alias="@type")
    result: str

class ReadAttributeAcceptResponseItem(AtType):
    type: Literal["ReadAttributeAcceptResponseItem"] = Field(default="ReadAttributeAcceptResponseItem", alias="@type")
    attribute: IdentityAttribute
    attributeId: str
    result: str

class IdentityAttribute(AtType):
    type: Literal["IdentityAttribute"] = Field(default="IdentityAttribute", alias="@type")
    owner: str
    value: Union[DisplayName, GivenName, Surname]

# Todo: add common base value type
class DisplayName(AtType):
    type: Literal["DisplayName"] = Field(default="DisplayName", alias="@type")
    value: str

class GivenName(AtType):
    type: Literal["GivenName"] = Field(default="GivenName", alias="@type")
    value: str

class Surname(AtType):
    type: Literal["Surname"] = Field(default="Surname", alias="@type")
    value: str

class RelationshipCreationChangeRequestContent(AtType):
    type: Literal["RelationshipCreationChangeRequestContent"] = Field(default="RelationshipCreationChangeRequestContent", alias="@type")
    response: RelationshipCreationChangeRequestContent_Response

class RelationshipCreationChangeRequestContent_Response(BaseModel):
    items: List[ResponseItemGroup]
    requestId: str
    result: str

class ResponseItemGroup(AtType):
    type: Literal["ResponseItemGroup"] = Field(default="ResponseItemGroup", alias="@type")
    # todo: add "any responseitem"
    items: List[Union[ShareAttributeAcceptResponseItem, ShareAttributeRejectResponseItem, ReadAttributeAcceptResponseItem, RejectResponseItem, Any]]

class ReadAttributeAcceptResponseItem(AtType):
    type: Literal["ReadAttributeAcceptResponseItem"] = Field(default="ReadAttributeAcceptResponseItem", alias="@type")
    attribute: IdentityAttribute
    attributeId: str
    result: str

class RejectResponseItem(AtType):
    type: Literal["RejectResponseItem"] = Field(default="RejectResponseItem", alias="@type")
    result: str


class ResponseWrapper(AtType):
    type: Literal["ResponseWrapper"] = Field(default="ResponseWrapper", alias="@type")
    requestId: str
    requestSourceReference: str
    requestSourceType: str
    response: ResponseWrapper_Response


class ResponseWrapper_Response(BaseModel):
    # TODO: can this be something else?
    items: List[ResponseItemGroup]

    # "@type": "ResponseWrapper",
    #   "requestId": "REQbdIZ2RxHfI8Ktye0g",
    #   "requestSourceReference": "RLTHFl5RKdd8qSqvc3J0",
    #   "requestSourceType": "RelationshipTemplate",
    #   "response"




    # @field_validator("items", mode="before")
    # def unpack_items(cls, v):
    #     return [my_factory(item) for item in v]
    #     # return [AtType.model_validate(v) for item in v]
    #     # return [ResponseItemGroup.model_validate(item) for item in v]

    # @validator("items", pre=True)
    # def unpack_items(cls, v):
    #     return [my_factory(item) for item in v]