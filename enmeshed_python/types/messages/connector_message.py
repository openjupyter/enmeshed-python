
from pydantic import BaseModel
from typing import List, Any

from .connector_message_recipient import ConnectorMessageRecipient
from ..unsorted import ResponseWrapper

class ConnectorMessage(BaseModel):
    id: str
    content: ResponseWrapper|Any
    createdBy: str
    createdByDevice: str
    recipients: List[ConnectorMessageRecipient]
    createdAt: str
    attachments: List[str]
    isOwn: bool

# import { ConnectorMessageRecipient } from "./ConnectorMessageRecipient";

# export interface ConnectorMessage {
#     id: string;
#     content: unknown;
#     createdBy: string;
#     createdByDevice: string;
#     recipients: ConnectorMessageRecipient[];
#     createdAt: string;
#     attachments: string[];
#     isOwn: boolean;
# }