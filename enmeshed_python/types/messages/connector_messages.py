from typing import List, TypeAlias
from .connector_message import ConnectorMessage

ConnectorMessages: TypeAlias = List[ConnectorMessage]


