
from typing import Optional
from pydantic import BaseModel


class ConnectorMessageRecipient(BaseModel):
    address: str
    relationshipId: str
    receivedAt: Optional[str] = None
    receivedByDevice: Optional[str] = None
