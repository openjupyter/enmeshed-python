from __future__ import annotations

from typing import Literal

from pydantic import BaseModel


class ConnectorHealthServices(BaseModel):
    database: Literal["healthy", "unhealthy"]
    backboneConnection: Literal["healthy", "unhealthy"]
    backboneAuthentication: Literal["healthy", "unhealthy"]


class ConnectorHealth(BaseModel):
    isHealthy: bool
    services: ConnectorHealthServices
