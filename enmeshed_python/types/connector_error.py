from __future__ import annotations

from typing import List, Optional

from pydantic import BaseModel


class ConnectorError(BaseModel):
    id: str
    code: str
    message: str
    docs: str
    time: str
    details: Optional[str] = None
    stacktrace: Optional[List[str]] = None
