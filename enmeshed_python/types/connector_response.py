from __future__ import annotations

from typing import Generic, Optional, TypeVar

from pydantic import BaseModel

from .connector_error import ConnectorError

T = TypeVar("T")


class ConnectorResponse(BaseModel, Generic[T]):
    error: Optional[ConnectorError] = None
    result: Optional[T] = None
