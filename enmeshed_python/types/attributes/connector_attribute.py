from __future__ import annotations

from datetime import datetime
from typing import Any, List, Literal, Optional, Union

from pydantic import BaseModel, Field


class ConnectorAttribute(BaseModel):
    id: str
    createdAt: datetime
    content: Union[ConnectorIdentityAttribute, ConnectorRelationshipAttribute]
    succeeds: Optional[str] = None
    succeededBy: Optional[str] = None
    shareInfo: Optional[ConnectorAttributeShareInfo] = None


class ConnectorAttributeShareInfoForRequest(BaseModel):
    requestReference: str
    peer: str
    sourceAttribute: Optional[str] = None


class ConnectorAttributeShareInfoForNotification(BaseModel):
    notificationReference: str
    peer: str
    sourceAttribute: Optional[str] = None


ConnectorAttributeShareInfo = Union[
    ConnectorAttributeShareInfoForRequest, ConnectorAttributeShareInfoForNotification
]


class ConnectorBaseAttribute(BaseModel):
    type: str = Field(alias="@type")
    owner: str
    validFrom: Optional[str] = None
    validTo: Optional[str] = None


class ConnectorIdentityAttribute(ConnectorBaseAttribute):
    type: Literal["IdentityAttribute"] = Field(alias="@type")
    value: ConnectorAttributeValue
    tags: Optional[List[str]] = None


class ConnectorRelationshipAttribute(ConnectorBaseAttribute):
    type: Literal["RelationshipAttribute"] = Field(alias="@type")
    value: ConnectorAttributeValue
    key: str
    isTechnical: Optional[bool] = None
    confidentiality: Literal["public", "private", "protected"]


class ConnectorAttributeValue(BaseModel):
    type: str = Field(alias="@type")
    value: Any


ConnectorAttributes = List[ConnectorAttribute]


class ConnectorHealthServices(BaseModel):
    database: Literal["healthy", "unhealthy"]
    backboneConnection: Literal["healthy", "unhealthy"]
    backboneAuthentication: Literal["healthy", "unhealthy"]


class ConnectorHealth(BaseModel):
    isHealthy: bool
    services: ConnectorHealthServices
