from typing import List, Literal, Optional
from pydantic import BaseModel, Field


class IdentityAttributeQuery(BaseModel):
    type: Optional[Literal["IdentityAttributeQuery"]] = Field(
        alias="@type", default="IdentityAttributeQuery"
    )
    valueType: str
    validFrom: Optional[str] = None
    validTo: Optional[str] = None
    tags: Optional[List[str]] = None
