from __future__ import annotations
from typing import List, Literal, Optional
from pydantic import BaseModel, Field


class IQLQuery(BaseModel):
    type: Literal["IQLQuery"] = Field(alias="@type", default="IQLQuery")
    queryString: str
    validFrom: Optional[str] = None
    validTo: Optional[str] = None
    attributeCreationHints: Optional[AttributeCreationHints] = None


class AttributeCreationHints(BaseModel):
    valueType: str
    tags: Optional[List[str]] = None
