from __future__ import annotations

from typing import List, Literal, Optional

from pydantic import BaseModel, Field


class ThirdPartyRelationshipAttributeQuery(BaseModel):
    type: Optional[Literal["ThirdPartyRelationshipAttributeQuery"]] = Field(
        alias="@type", default="ThirdPartyRelationshipAttributeQuery"
    )
    key: str
    owner: str
    thirdParty: List[str]
    validFrom: Optional[str] = None
    validTo: Optional[str] = None
