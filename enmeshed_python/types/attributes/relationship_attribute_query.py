from __future__ import annotations
from typing import List, Literal, Optional, Union
from pydantic import BaseModel, Field


class RelationshipAttributeQuery(BaseModel):
    type: Optional[Literal["RelationshipAttributeQuery"]] = Field(
        alias="@type", default="RelationshipAttributeQuery"
    )
    validFrom: Optional[str] = None
    validTo: Optional[str] = None
    key: str
    owner: str
    attributeCreationHints: RelationshipAttributeCreationHints


class RelationshipAttributeCreationHints(BaseModel):
    title: str
    valueType: str
    description: Optional[str] = None
    valueHints: Optional[ValueHints] = None
    isTechnical: Optional[bool] = None
    confidentiality: Literal["public", "private", "protected"]


class ValueHints(BaseModel):
    type: Literal["ValueHints"] = Field(alias="@type", default="ValueHints")
    editHelp: Optional[str] = None
    # should this be int or float?
    min: Optional[float] = None
    max: Optional[float] = None
    pattern: Optional[str] = None
    values: Optional[List[ValueHintsValue]] = None
    defaultValue: Optional[Union[str, float, bool]] = None


class ValueHintsValue(BaseModel):
    type: Literal["ValueHintsValue"] = Field(alias="@type", default="ValueHintsValue")
    key: Union[str, float, bool]
    displayName: str
