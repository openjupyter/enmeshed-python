from __future__ import annotations
from typing import Optional, List
from pydantic import BaseModel


class ConnectorRequestValidationResult(BaseModel):
    isSuccess: bool
    code: Optional[str] = None
    message: Optional[str] = None
    items: List[ConnectorRequestValidationResult]
