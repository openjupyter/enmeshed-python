from __future__ import annotations

from typing import Optional

from pydantic import BaseModel
from ...relationship_templates import ConnectorRelationshipTemplateContent


class CreateOwnRelationshipTemplateRequest(BaseModel):
    maxNumberOfAllocations: Optional[int] = None
    expiresAt: str
    content: ConnectorRelationshipTemplateContent | dict
