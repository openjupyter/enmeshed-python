from ..connector_request_content import ConnectorRequestContent
from pydantic import BaseModel


class CreateOutgoingRequestRequest(BaseModel):
    content: ConnectorRequestContent
    peer: str
