from ..connector_request_content import ConnectorRequestContent
from pydantic import BaseModel
from typing import Optional


class CanCreateOutgoingRequestRequest(BaseModel):
    content: ConnectorRequestContent
    peer: Optional[str] = None
