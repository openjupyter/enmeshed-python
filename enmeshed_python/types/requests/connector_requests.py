from typing import List, TypeAlias
from .connector_request import ConnectorRequest

ConnectorRequests: TypeAlias = List[ConnectorRequest]
