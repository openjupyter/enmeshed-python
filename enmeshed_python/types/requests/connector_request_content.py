from __future__ import annotations

from typing import Any, List, Literal, Optional, Union

from pydantic import BaseModel, Field

# from ..attributes.connector_attribute import
from ..attributes import (
    ConnectorIdentityAttribute,
    ConnectorRelationshipAttribute,
    IdentityAttributeQuery,
    IQLQuery,
    RelationshipAttributeQuery,
    ThirdPartyRelationshipAttributeQuery,
)

# export interface ConnectorRequestContent {
#     "@type"?: string;
#     id?: string;
#     expiresAt?: string;
#     items: (CreateOutgoingRequestRequestContentItemDerivations | ConnectorRequestContentItemGroup)[];
#     title?: string;
#     description?: string;
#     metadata?: object;
# }

# export interface ConnectorRequestContentItemGroup {
#     "@type"?: "RequestItemGroup";
#     title?: string;
#     description?: string;
#     mustBeAccepted: boolean;
#     metadata?: object;
#     items: CreateOutgoingRequestRequestContentItemDerivations[];
# }

# export type CreateOutgoingRequestRequestContentItemDerivations =
#     | CreateAttributeRequestItem
#     | ShareAttributeRequestItem
#     | ProposeAttributeRequestItem
#     | ReadAttributeRequestItem
#     | ConsentRequestItem
#     | AuthenticationRequestItem
#     | FreeTextRequestItem
#     | RegisterAttributeListenerRequestItem;

# export interface ConnectorRequestContentItem {
#     "@type"?: string;
#     title?: string;
#     description?: string;
#     responseMetadata?: object;
#     mustBeAccepted: boolean;
# }

# export interface CreateAttributeRequestItem extends ConnectorRequestContentItem {
#     "@type": "CreateAttributeRequestItem";
#     attribute: ConnectorIdentityAttribute | ConnectorRelationshipAttribute;
# }

# export interface ShareAttributeRequestItem extends ConnectorRequestContentItem {
#     "@type": "ShareAttributeRequestItem";
#     attribute: ConnectorIdentityAttribute | ConnectorRelationshipAttribute;
#     sourceAttributeId: string;
# }

# export interface ProposeAttributeRequestItem extends ConnectorRequestContentItem {
#     "@type": "ProposeAttributeRequestItem";
#     query: IdentityAttributeQuery | RelationshipAttributeQuery | IQLQuery;
#     attribute: ConnectorIdentityAttribute | ConnectorRelationshipAttribute;
# }

# export interface ReadAttributeRequestItem extends ConnectorRequestContentItem {
#     "@type": "ReadAttributeRequestItem";
#     query: IdentityAttributeQuery | RelationshipAttributeQuery | ThirdPartyRelationshipAttributeQuery | IQLQuery;
# }

# export interface ConsentRequestItem extends ConnectorRequestContentItem {
#     "@type": "ConsentRequestItem";
#     consent: string;
#     link?: string;
# }

# export interface AuthenticationRequestItem extends ConnectorRequestContentItem {
#     "@type": "AuthenticationRequestItem";
# }

# export interface FreeTextRequestItem extends ConnectorRequestContentItem {
#     "@type": "FreeTextRequestItem";
#     freeText: string;
# }

# export interface RegisterAttributeListenerRequestItem extends ConnectorRequestContentItem {
#     "@type": "RegisterAttributeListenerRequestItem";
#     query: IdentityAttributeQuery | ThirdPartyRelationshipAttributeQuery;
# }


class ConnectorRequestContent(BaseModel):
    type: Optional[str] = Field(alias="@type", default=None)
    id: Optional[str] = None
    expiresAt: Optional[str] = None
    items: List[
        Union[
            CreateOutgoingRequestRequestContentItemDerivations,
            ConnectorRequestContentItemGroup,
        ]
    ]
    title: Optional[str] = None
    description: Optional[str] = None
    metadata: Optional[object] = None


class ConnectorRequestContentItemGroup(BaseModel):
    type: Optional[Literal["RequestItemGroup"]] = Field(
        alias="@type", default="RequestItemGroup"
    )
    title: Optional[str] = None
    description: Optional[str] = None
    mustBeAccepted: bool
    metadata: Optional[object] = None
    items: List[CreateOutgoingRequestRequestContentItemDerivations]


# CreateOutgoingRequestRequestContentItemDerivations = dict


class ConnectorRequestContentItem(BaseModel):
    type: Optional[str] = Field(alias="@type", default=None)
    title: Optional[str] = None
    description: Optional[str] = None
    responseMetadata: Optional[object] = None
    mustBeAccepted: bool


class CreateAttributeRequestItem(ConnectorRequestContentItem):
    type: Literal["CreateAttributeRequestItem"] = Field(
        alias="@type", default="CreateAttributeRequestItem"
    )
    attribute: Union[ConnectorIdentityAttribute, ConnectorRelationshipAttribute]


class ShareAttributeRequestItem(ConnectorRequestContentItem):
    type: Literal["ShareAttributeRequestItem"] = Field(
        alias="@type", default="ShareAttributeRequestItem"
    )
    attribute: Union[ConnectorIdentityAttribute, ConnectorRelationshipAttribute]
    sourceAttributeId: str


class ProposeAttributeRequestItem(ConnectorRequestContentItem):
    type: Literal["ProposeAttributeRequestItem"] = Field(
        alias="@type", default="ProposeAttributeRequestItem"
    )
    query: Union[IdentityAttributeQuery, RelationshipAttributeQuery, IQLQuery]
    attribute: Union[ConnectorIdentityAttribute, ConnectorRelationshipAttribute]


class ReadAttributeRequestItem(ConnectorRequestContentItem):
    type: Literal["ReadAttributeRequestItem"] = Field(
        alias="@type", default="ReadAttributeRequestItem"
    )
    query: Union[
        IdentityAttributeQuery,
        RelationshipAttributeQuery,
        ThirdPartyRelationshipAttributeQuery,
        IQLQuery,
    ]


class ConsentRequestItem(ConnectorRequestContentItem):
    type: Literal["ConsentRequestItem"] = Field(
        alias="@type", default="ConsentRequestItem"
    )
    consent: str
    link: Optional[str] = None


class AuthenticationRequestItem(ConnectorRequestContentItem):
    type: Literal["AuthenticationRequestItem"] = Field(
        alias="@type", default="AuthenticationRequestItem"
    )


class FreeTextRequestItem(ConnectorRequestContentItem):
    type: Literal["FreeTextRequestItem"] = Field(
        alias="@type", default="FreeTextRequestItem"
    )
    freeText: str


class RegisterAttributeListenerRequestItem(ConnectorRequestContentItem):
    type: Literal["RegisterAttributeListenerRequestItem"] = Field(
        alias="@type", default="RegisterAttributeListenerRequestItem"
    )
    query: Union[IdentityAttributeQuery, ThirdPartyRelationshipAttributeQuery]


CreateOutgoingRequestRequestContentItemDerivations = Union[
    CreateAttributeRequestItem,
    ShareAttributeRequestItem,
    ProposeAttributeRequestItem,
    ReadAttributeRequestItem,
    ConsentRequestItem,
    AuthenticationRequestItem,
    FreeTextRequestItem,
    RegisterAttributeListenerRequestItem,
]
# CreateOutgoingRequestRequestContentItemDerivations = TypeAdapter(CreateOutgoingRequestRequestContentItemDerivationsTyp)
# CreateOutgoingRequestRequestContentItemDerivations = Any


# tmp
# ConnectorIdentityAttribute = Any
# ConnectorRelationshipAttribute = Any
# IdentityAttributeQuery = Any
# IQLQuery = Any
# RelationshipAttributeQuery = Any
# ThirdPartyRelationshipAttributeQuery = Any
