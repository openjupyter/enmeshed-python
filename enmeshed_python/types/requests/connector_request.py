from __future__ import annotations

from typing import List, Literal, Optional, Union

from pydantic import BaseModel, Field

from .connector_request_content import ConnectorRequestContent


class ConnectorRequest(BaseModel):
    id: str
    isOwn: bool
    peer: str
    createdAt: str
    status: ConnectorRequestStatus
    content: ConnectorRequestContent
    source: Optional[ConnectorRequestSource] = None
    response: Optional[ConnectorRequestResponse] = None


ConnectorRequestStatus = Literal[
    "Draft",
    "Open",
    "DecisionRequired",
    "ManualDecisionRequired",
    "Decided",
    "Completed",
]


class ConnectorRequestSource(BaseModel):
    type: Literal["Message", "RelationshipTemplate"]
    reference: str


class ConnectorRequestResponse(BaseModel):
    createdAt: str
    content: ConnectorRequestResponseContent
    source: Optional[ConnectorRequestResponseSource] = None


class ConnectorRequestResponseSource(BaseModel):
    type: Literal["Message", "RelationshipChange"]
    reference: str


class ConnectorRequestResponseContent(BaseModel):
    type: str = Field(alias="@type")
    result: ConnectorRequestResponseResult
    requestId: str
    items: List[Union[ConnectorRequestResponseItemGroup, ConnectorRequestResponseItem]]


ConnectorRequestResponseResult = Literal["Accepted", "Rejected"]


class ConnectorRequestResponseItem(BaseModel):
    type: str = Field(alias="@type")
    result: ConnectorRequestResponseItemResult
    metadata: Optional[object] = None


ConnectorRequestResponseItemResult = Literal["Accepted", "Rejected", "Error"]


class ConnectorRequestResponseItemGroup(BaseModel):
    type: str = Field(alias="@type")
    items: List[ConnectorRequestResponseItem]
    metadata: Optional[object] = None
