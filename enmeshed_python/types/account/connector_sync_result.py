
from pydantic import BaseModel
from typing import List
from enmeshed_python.types.messages import ConnectorMessage
from enmeshed_python.types.relationships import ConnectorRelationship

class ConnectorSyncResult(BaseModel):
    messages: List[ConnectorMessage]
    relationships: List[ConnectorRelationship]