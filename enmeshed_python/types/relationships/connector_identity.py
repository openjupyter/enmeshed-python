from __future__ import annotations
from pydantic import BaseModel


class ConnectorIdentity(BaseModel):
    address: str
    publicKey: str
    realm: str
