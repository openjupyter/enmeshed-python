from __future__ import annotations

from pydantic import BaseModel

from .connector_identity import ConnectorIdentity
from ..relationship_templates import ConnectorRelationshipTemplate
from .connector_relationship_status import ConnectorRelationshipStatus
from .connector_relationship_changes import ConnectorRelationshipChanges


class ConnectorRelationship(BaseModel):    
    template: ConnectorRelationshipTemplate
    status: ConnectorRelationshipStatus
    peer: str
    id: str
    peerIdentity: ConnectorIdentity
    changes: ConnectorRelationshipChanges
