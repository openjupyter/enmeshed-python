
from typing import Any, Optional
from pydantic import BaseModel

class ConnectorRelationshipChangeResponse(BaseModel):
    createdBy: str
    createdByDevice: str
    createdAt: str
    content: Optional[Any] = None
