from enum import Enum

class ConnectorRelationshipStatus(str, Enum):
    PENDING = "Pending"
    ACTIVE = "Active"
    REJECTED = "Rejected"
