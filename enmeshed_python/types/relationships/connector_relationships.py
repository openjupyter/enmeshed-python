from typing import TypeAlias, List
from .connector_relationship import ConnectorRelationship

ConnectorRelationships: TypeAlias = List[ConnectorRelationship]