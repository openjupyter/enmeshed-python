from __future__ import annotations

from typing import Optional

from pydantic import BaseModel

from .connector_relationship_change_request import ConnectorRelationshipChangeRequest
from .connector_relationship_change_response import ConnectorRelationshipChangeResponse
from .connector_relationship_change_status import ConnectorRelationshipChangeStatus
from .connector_relationship_change_type import ConnectorRelationshipChangeType


class ConnectorRelationshipChange(BaseModel):
    id: str
    type: ConnectorRelationshipChangeType
    status: ConnectorRelationshipChangeStatus
    request: ConnectorRelationshipChangeRequest
    response: Optional[ConnectorRelationshipChangeResponse] = None
