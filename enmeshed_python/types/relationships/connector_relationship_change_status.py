from enum import Enum

class ConnectorRelationshipChangeStatus(str, Enum):
    PENDING = "Pending"
    REJECTED = "Rejected"
    ACCEPTED = "Accepted"
