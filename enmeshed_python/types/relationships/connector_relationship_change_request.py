from pydantic import BaseModel
from typing import Optional, Any
from enmeshed_python.types.attypes import AtType

class ConnectorRelationshipChangeRequest(BaseModel):
    createdBy: str
    createdByDevice: str
    createdAt: str
    # Any must come last, otherwise it will parse as a dict
    content: Optional[AtType|Any] = None
