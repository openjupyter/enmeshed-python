from enum import Enum

class ConnectorRelationshipChangeType(str, Enum):
    CREATION = "Creation"
