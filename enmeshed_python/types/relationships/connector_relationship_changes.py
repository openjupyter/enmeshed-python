from typing import TypeAlias, List
from .connector_relationship_change import ConnectorRelationshipChange

ConnectorRelationshipChanges: TypeAlias = List[ConnectorRelationshipChange]