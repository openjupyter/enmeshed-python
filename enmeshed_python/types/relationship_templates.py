from __future__ import annotations
from .requests.connector_request_content import ConnectorRequestContent
from pydantic import BaseModel, Field
from typing import Literal, Optional, Union, Mapping, Any


class ConnectorRelationshipTemplate(BaseModel):
    id: str
    isOwn: bool
    maxNumberOfAllocations: Optional[int] = None
    createdBy: str
    createdByDevice: str
    createdAt: str
    content: Union[ConnectorRelationshipTemplateContent, Mapping[str, Any]]
    expiresAt: Optional[str] = None
    secretKey: str
    truncatedReference: str


class ConnectorRelationshipTemplateContent(BaseModel):
    type: Literal["RelationshipTemplateContent"] = Field(
        alias="@type", default="RelationshipTemplateContent"
    )
    title: Optional[str] = None
    metadata: Optional[Mapping[str, Any]] = None
    onNewRelationship: ConnectorRequestContent
    onExistingRelationship: Optional[ConnectorRequestContent] = None
