from pydantic import TypeAdapter

from enmeshed_python.endpoints.endpoint import Endpoint
from enmeshed_python.types import ConnectorResponse
from enmeshed_python.types.requests import (
    CanCreateOutgoingRequestRequest,
    ConnectorRequest,
    ConnectorRequests,
    ConnectorRequestValidationResult,
    CreateOutgoingRequestRequest,
    GetOutgoingRequestsRequest,
)


# Corresponds to:
# https://github.com/nmshd/connector/blob/main/packages/sdk/src/endpoints/OutgoingRequestsEndpoint.ts
class OutgoingRequestsEndpoint(Endpoint):
    def can_create_request(
        self, request: CanCreateOutgoingRequestRequest
    ) -> ConnectorRequestValidationResult:
        url = "/api/v2/Requests/Outgoing/Validate"

        adapter = TypeAdapter(CanCreateOutgoingRequestRequest)
        data = adapter.dump_python(request, exclude_none=True, by_alias=True)
        response = self.client.post(url, json=data)
        res_adapter = TypeAdapter(ConnectorResponse[ConnectorRequestValidationResult])
        res = res_adapter.validate_json(response.text)
        if not res.result:
            raise RuntimeError(res.error)
        return res.result

    def create_request(self, request: CreateOutgoingRequestRequest) -> ConnectorRequest:
        url = "/api/v2/Requests/Outgoing"
        adapter = TypeAdapter(CreateOutgoingRequestRequest)
        data = adapter.dump_python(request, exclude_none=True, by_alias=True)
        response = self.client.post(url, json=data)
        res_adapter = TypeAdapter(ConnectorResponse[ConnectorRequest])
        res = res_adapter.validate_json(response.text)
        if not res.result:
            raise RuntimeError(res.error)
        return res.result

    def get_requests(self, request: GetOutgoingRequestsRequest) -> ConnectorRequests:
        url = "/api/v2/Requests/Outgoing"
        request_dict = TypeAdapter(GetOutgoingRequestsRequest).dump_python(
            request, exclude_none=True, by_alias=True
        )
        url += "?" + dict_to_query_string(flatten_dict(request_dict))
        print("New url:", url)
        response = self.client.get(url)
        response.raise_for_status()
        parsed = TypeAdapter(ConnectorResponse[ConnectorRequests]).validate_json(
            response.text
        )
        print("status code:", response.status_code)
        print("parsed:", parsed.result)
        if parsed.result is None:
            raise RuntimeError(parsed.error)
        return parsed.result


def flatten_dict(d, parent_key="", sep="."):
    items = []
    for k, v in d.items():
        new_key = f"{parent_key}{sep}{k}" if parent_key else k
        if isinstance(v, dict):
            items.extend(flatten_dict(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


def dict_to_query_string(params):
    from urllib.parse import urlencode

    return urlencode(params, doseq=True)
