
from enmeshed_python.endpoints.endpoint import Endpoint
from enmeshed_python.types import ConnectorHealth

class MonitoringEndpoint(Endpoint):
    def get_health(self) -> ConnectorHealth:
        """Returns information about the connector service health."""

        url = "/health"
        response = self.client.get(url)
        response.raise_for_status()
        return ConnectorHealth.model_validate_json(response.text)
