
from typing import Optional

from pydantic import TypeAdapter

from enmeshed_python.endpoints.endpoint import Endpoint
from enmeshed_python.types import ConnectorResponse
from enmeshed_python.types.attributes import ConnectorAttributes


# Corresponds to:
# https://github.com/nmshd/connector/blob/main/packages/sdk/src/endpoints/AttributesEndpoint.ts
class AttributesEndpoint(Endpoint):
    """ Methods for managing attributes. """

    # TODO: add parameters:
    def get_attributes(self) -> ConnectorAttributes:
        """ Returns a list of attributes. """
        
        url = "/api/v2/Attributes"
        # with self.client as client:
        response = self.client.get(url)
        response.raise_for_status()
        parsed = TypeAdapter(ConnectorResponse[ConnectorAttributes]).validate_json(
            response.text
        )
        if not parsed.result:
            raise RuntimeError(parsed.error)
        return parsed.result

    # TODO: add other params from
    # https://github.com/nmshd/connector/blob/main/packages/sdk/src/types/attributes/requests/GetPeerSharedIdentityAttributesRequest.ts
    def get_peer_shared_identity_attributes(
            self,
            peer: str,
            # TODO: what is the default? Optional[bool] does not make so much sense
            onlyLatestVersions: Optional[bool] = None
    ) -> ConnectorAttributes:
        """ Get attributes that have been shared by a peer. """

        url = "/api/v2/Attributes/Peer/Shared/Identity"
        args = []
        args.append(f"peer={peer}")
        if onlyLatestVersions is not None:
            args.append(f"onlyLatestVersions={'true' if onlyLatestVersions else 'false'}")
        if args:
            url += "?" + "&".join(args)
        return self.get(url, ConnectorAttributes)
