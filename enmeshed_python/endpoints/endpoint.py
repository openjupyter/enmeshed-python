
from typing import Optional, Type, TypeVar
import httpx
from pydantic import TypeAdapter
from pydantic import ValidationError
from enmeshed_python.types import ConnectorResponse

T = TypeVar('T')        

class Endpoint:
    def __init__(self, client: httpx.Client):
        self.client = client

    # TODO: add args
    def get(self, url: str, return_type: Type[T]) -> T:
        response = self.client.get(url)
        response.raise_for_status()
        adapter: TypeAdapter[ConnectorResponse[T]] = TypeAdapter(ConnectorResponse[return_type])  # type: ignore
        # print(response.text)
        try:
            parsed = adapter.validate_json(response.text)
        except ValidationError as e:
            print("A validation error occurred. Could not parse the following as %s" % ConnectorResponse[return_type])
            print(response.text)
            raise
        if parsed.error is not None:
            raise RuntimeError(parsed.error)
        return parsed.result

    # TODO: add args
    def post(self, url: str, return_type: Type[T]) -> T:
        response = self.client.post(url)
        response.raise_for_status()
        adapter: TypeAdapter[ConnectorResponse[T]] = TypeAdapter(ConnectorResponse[return_type])  # type: ignore
        parsed = adapter.validate_json(response.text)
        if parsed.error is not None:
            raise RuntimeError(parsed.error)
        return parsed.result

    def put(self, url: str, return_type: Type[T], json: Optional[str]=None) -> T:
        response = self.client.put(url, json=json)
        if response.status_code >= 400:
            print(response.text)
        response.raise_for_status()
        adapter: TypeAdapter[ConnectorResponse[T]] = TypeAdapter(ConnectorResponse[return_type])  # type: ignore
        parsed = adapter.validate_json(response.text)
        if parsed.error is not None:
            raise RuntimeError(parsed.error)
        return parsed.result


def build_args(argname: str, args: Optional[str | list[str]]=None) -> list[str]:
    if args is None:
        return []
    if isinstance(args, list):
        return [f"{argname}={x}" for x in args]
    return [f"{argname}={args}"]