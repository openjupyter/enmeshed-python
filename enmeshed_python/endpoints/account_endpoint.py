
from enmeshed_python.endpoints.endpoint import Endpoint
from enmeshed_python.types.account import ConnectorSyncResult

class AccountEndpoint(Endpoint):
    # TODO: getIdentityInfo

    def sync(self) -> ConnectorSyncResult:
        url = "/api/v2/Account/Sync"
        return self.post(url, ConnectorSyncResult)

    # TODO: getSyncInfo