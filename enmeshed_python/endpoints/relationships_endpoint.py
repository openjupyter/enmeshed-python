from typing import Optional
from enmeshed_python.types.relationships import (
    ConnectorRelationships,
    ConnectorRelationship,
)
from enmeshed_python.endpoints.endpoint import Endpoint, build_args


class RelationshipsEndpoint(Endpoint):
    """Methods for managing relationships."""

    # TODO: add other arguments
    # https://github.com/nmshd/connector/blob/0f63cd3aace4790e4922a787d974db81d1124c14/packages/sdk/src/types/relationships/requests/GetRelationshipsRequest.ts#L3
    def get_relationships(
        self,
        template_id: Optional[str | list[str]] = None,
        peer: Optional[str | list[str]] = None,
        status: Optional[str | list[str]] = None,
    ) -> ConnectorRelationships:
        """Get a list of relationships."""

        url = "/api/v2/Relationships"
        args = []
        # TODO: build query string in a safe way
        args += build_args("template.id", template_id)
        args += build_args("peer", peer)
        args += build_args("status", status)
        if args:
            url += "?" + "&".join(args)
        # print("New url:", url)
        return self.get(url, ConnectorRelationships)

    # TODO: how does the "request" parameter work?

    # public async acceptRelationshipChange(
    #     relationshipId: string,
    #     changeId: string,
    #     request: AcceptRelationshipChangeRequest = { content: {} }
    # ): Promise<ConnectorResponse<ConnectorRelationship>> {
    #     return await this.put(`/api/v2/Relationships/${relationshipId}/Changes/${changeId}/Accept`, request);
    # }

    def accept_relationship_change(
        self, relationship_id: str, change_id: str, content: Optional[dict] = None
    ) -> ConnectorRelationship:
        """
        Accepts the change with the given `change_id`. If the change exists
        but belongs to another relationship, this call will fail and raise
        an exception.

        :param relationship_id: The relationship ID.
        :param change_id: The change ID.
        :param content: An optional answer to the relationship change which the peer can receive.
        :return: The updated relationship.
        :raises: HttpStatusError: if the request fails.
        """

        url = f"/api/v2/Relationships/{relationship_id}/Changes/{change_id}/Accept"
        if content is None:
            content = {}
        data = {"content": content}
        return self.put(url, ConnectorRelationship, json=data)
