
from typing import List, Optional, TypeAlias
from enmeshed_python.endpoints.endpoint import Endpoint, build_args
from enmeshed_python.types.messages import ConnectorMessages


StrOrList: TypeAlias = str | List[str]

class MessagesEndpoint(Endpoint):

    def get_messages(
            self,
            createdBy: Optional[StrOrList] = None,
            createdAt: Optional[StrOrList] = None,
            recipientsAddress: Optional[StrOrList] = None,
            recipientsRelationshipId: Optional[StrOrList] = None,
            participant: Optional[StrOrList] = None,
            attachments: Optional[StrOrList] = None,
            contentType: Optional[StrOrList] = None,
            contentSubject: Optional[StrOrList] = None,
            contentBody: Optional[StrOrList] = None
    ):
        url = "/api/v2/Messages"
        args = []
        args += build_args("createdBy", createdBy)
        args += build_args("createdAt", createdAt)
        args += build_args("recipients.address", recipientsAddress)
        args += build_args("recipients.relationshipId", recipientsRelationshipId)
        args += build_args("participant", participant)
        args += build_args("attachments", attachments)
        args += build_args("content.@type", contentType)
        args += build_args("content.subject", contentSubject)
        args += build_args("content.body", contentBody)
        if args:
            url += "?" + "&".join(args)

        return self.get(url, ConnectorMessages)