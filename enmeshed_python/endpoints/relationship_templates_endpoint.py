from typing import Optional, TypeAlias, Union, overload

from pydantic import TypeAdapter

from enmeshed_python.endpoints.endpoint import Endpoint
from enmeshed_python.types.connector_response import ConnectorResponse
from enmeshed_python.types.relationship_templates import (
    ConnectorRelationshipTemplate, ConnectorRelationshipTemplateContent)
from enmeshed_python.types.requests.requests import \
    CreateOwnRelationshipTemplateRequest


class RelationshipTemplatesEndpoint(Endpoint):
    # getRelationshipTemplates
    # getRelationshipTemplate
    # getOwnRelationshipTemplates

    # createOwnRelationshipTemplate
    # def create_own_relationship_template(
    #     self, request: CreateOwnRelationshipTemplateRequest
    # ) -> ConnectorRelationshipTemplate:
    #     url = "/api/v2/RelationshipTemplates/Own"
    #     adapter = TypeAdapter(CreateOwnRelationshipTemplateRequest)
    #     data = adapter.dump_python(request, exclude_none=True, by_alias=True)
    #     response = self.client.post(url, json=data)
    #     res_adapter = TypeAdapter(ConnectorResponse[ConnectorRelationshipTemplate])
    #     res = res_adapter.validate_json(response.text)
    #     if not res.result:
    #         raise RuntimeError(res.error)
    #     return res.result

    content_type: TypeAlias = ConnectorRelationshipTemplateContent | dict

    @overload
    def create_own_relationship_template(
        self, content: content_type, expiresAt: str, maxNumberOfAllocations: Optional[int] = None
    ) -> ConnectorRelationshipTemplate: ...

    @overload
    def create_own_relationship_template(
        self, request: CreateOwnRelationshipTemplateRequest
    ) -> ConnectorRelationshipTemplate: ...

    def create_own_relationship_template(
        self,
        content: Union[CreateOwnRelationshipTemplateRequest, content_type],
        expiresAt: Optional[str] = None,
        maxNumberOfAllocations: Optional[int] = None,
        request: Optional[CreateOwnRelationshipTemplateRequest] = None,
    ) -> ConnectorRelationshipTemplate:
        firstarg = content

        if request is not None:
            # pass request object by kwarg
            pass
        elif isinstance(firstarg, CreateOwnRelationshipTemplateRequest):
            # pass request object as first argument
            request = firstarg
        else:
            # pass content (dict or object) as first argument or content kwarg
            request = CreateOwnRelationshipTemplateRequest(
                maxNumberOfAllocations=maxNumberOfAllocations,
                expiresAt=expiresAt,
                content=content
            )

        url = "/api/v2/RelationshipTemplates/Own"
        adapter = TypeAdapter(CreateOwnRelationshipTemplateRequest)
        data = adapter.dump_python(request, exclude_none=True, by_alias=True)
        response = self.client.post(url, json=data)
        res_adapter = TypeAdapter(
            ConnectorResponse[ConnectorRelationshipTemplate])
        res = res_adapter.validate_json(response.text)
        if not res.result:
            raise RuntimeError(res.error)
        return res.result

    # getQrCodeForOwnRelationshipTemplate
    def get_qr_code_for_own_relationship_template(self, template_id: str):
        url = f"/api/v2/RelationshipTemplates/{template_id}"
        headers = {"Accept": "image/png"}
        response = self.client.get(url, headers=headers)
        response.raise_for_status()
        # TODO: return the content instead of saving
        # Save content as image
        with open("qr_code.png", "wb") as f:
            f.write(response.content)

    # createTokenForOwnRelationshipTemplate
    # createTokenQrCodeForOwnRelationshipTemplate
    # getPeerRelationshipTemplates
    # loadPeerRelationshipTemplate
